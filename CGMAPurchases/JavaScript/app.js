﻿var emailPurchases = angular.module('EmailPurchases', ['ui.bootstrap']);
emailPurchases.controller('EmailController', function ($scope, $http, $window) { 

    $scope.itemsPerPage = 10;  
    $scope.currentPage;
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.typeOptions = [
        { name: 'Email', value: 'email' },
        { name: 'CIMA ID', value: 'cimaid' }
    ];

    $scope.form = { type: $scope.typeOptions[0].value };

    $scope.ButtonClick = function () {
        var post = $http({
            method: "POST",
            url: "/Home/GetPurchasesByEmail",
            dataType: 'json',
            data: { email: $scope.SearchValue, searchtype: $scope.form.type },
            headers: { "Content-Type": "application/json" }
        });

        post.then(function (result) {
            $scope.purchaseResults = result.data;
        }).catch(function (result) {
            console.log(result.status);
        }); 
    }

});

emailPurchases.filter('startFrom', function () {
    return function (data, start) {
        return data.slice(start);
    }
})

emailPurchases.filter("dateFormat", function () {
    return function (x) {
        return new Date(parseInt(x.substr(6)));
    };
});
