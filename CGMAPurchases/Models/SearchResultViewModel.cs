﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CGMAPurchases.Models
{
    public class SearchResultViewModel
    {
        public string CIMAContactId { get; set; }
        public int PurchaseId { get; set; }
        public int ProductPurchaseId { get; set; }
        public string ProductPurchaseName { get; set; } 
        public string ProductPurchaseType { get; set; }
        public string EmailAddress { get; set; } 
        public int TransactionID { get; set; } 
        public string TransactionState { get; set; }
        public Guid TransactionRef { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}