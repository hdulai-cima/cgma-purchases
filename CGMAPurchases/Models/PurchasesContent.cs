namespace CGMAPurchases.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PurchasesContent : DbContext
    {
        public PurchasesContent()
            : base("name=PurchasesContent")
        {
        }

        public virtual DbSet<tblProductPurchaseSummary> tblProductPurchaseSummary { get; set; }
        public virtual DbSet<tblTransaction> tblTransaction { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.ProductPurchaseName)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.ProductPurchaseType)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.PaymentMethod)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.CIMAContactId)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.Firstname)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.Middlename)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.Lastname)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.Nationality)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.AddressLine1)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.AddressLine2)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.AddressCity)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.AddressRegion)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.AddressCountry)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.AddressPostcode)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.EmailAddress)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.Telephone)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.DiscountCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.CompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.JobTitle)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.CompanyCountry)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.TuitionProvider)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.FulfilmentMethod)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.CurrencyCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.CorporateDiscountCode)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.PurchaseOrderNumber)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .Property(e => e.UpdatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<tblProductPurchaseSummary>()
                .HasMany(e => e.tblTransaction)
                .WithMany(e => e.tblProductPurchaseSummary)
                .Map(m => m.ToTable("tblProductPurchaseSummary_to_tblTransaction").MapLeftKey("PurchaseId").MapRightKey("TransactionID"));

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.TransactionType)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.TransactionState)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.Decision)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.ReasonCode)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.ResponseData)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.ReconciliationID)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.BillingCompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.BillingAddress1)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.BillingAddress2)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.BillingAddress3)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.BillingCity)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.BillingRegion)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.BillingCountry)
                .IsUnicode(false);

            modelBuilder.Entity<tblTransaction>()
                .Property(e => e.BillingPostcode)
                .IsUnicode(false);
        }
    }
}
