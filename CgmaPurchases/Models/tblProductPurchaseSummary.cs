namespace CGMAPurchases.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblProductPurchaseSummary")]
    public partial class tblProductPurchaseSummary
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblProductPurchaseSummary()
        {
            tblTransaction = new HashSet<tblTransaction>();
        }

        public int Id { get; set; }

        public int ProductPurchaseId { get; set; }

        [StringLength(100)]
        public string ProductPurchaseName { get; set; }

        [StringLength(50)]
        public string ProductPurchaseType { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(20)]
        public string PaymentMethod { get; set; }

        [StringLength(20)]
        public string CIMAContactId { get; set; }

        [StringLength(20)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Firstname { get; set; }

        [StringLength(50)]
        public string Middlename { get; set; }

        [StringLength(50)]
        public string Lastname { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [StringLength(50)]
        public string Gender { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }

        [StringLength(100)]
        public string AddressLine1 { get; set; }

        [StringLength(100)]
        public string AddressLine2 { get; set; }

        [StringLength(100)]
        public string AddressCity { get; set; }

        [StringLength(100)]
        public string AddressRegion { get; set; }

        [StringLength(100)]
        public string AddressCountry { get; set; }

        [StringLength(100)]
        public string AddressPostcode { get; set; }

        [StringLength(200)]
        public string EmailAddress { get; set; }

        [StringLength(30)]
        public string Telephone { get; set; }

        [StringLength(30)]
        public string DiscountCode { get; set; }

        [StringLength(100)]
        public string CompanyName { get; set; }

        [StringLength(100)]
        public string JobTitle { get; set; }

        [StringLength(100)]
        public string CompanyCountry { get; set; }

        [StringLength(30)]
        public string TuitionProvider { get; set; }

        [StringLength(20)]
        public string FulfilmentMethod { get; set; }

        public bool? CanContactByCIMA { get; set; }

        public bool? CanContactByExternal { get; set; }

        [Column(TypeName = "xml")]
        public string AdditionalQuestions { get; set; }

        [StringLength(10)]
        public string CurrencyCode { get; set; }

        [StringLength(50)]
        public string CorporateDiscountCode { get; set; }

        [StringLength(50)]
        public string PurchaseOrderNumber { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public bool? IsGuest { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblTransaction> tblTransaction { get; set; }
    }
}
