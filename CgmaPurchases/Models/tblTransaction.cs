namespace CGMAPurchases.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblTransaction")]
    public partial class tblTransaction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblTransaction()
        {
            tblProductPurchaseSummary = new HashSet<tblProductPurchaseSummary>();
        }

        [Key]
        public int TransactionID { get; set; }

        [Required]
        [StringLength(20)]
        public string TransactionType { get; set; }

        public Guid TransactionRef { get; set; }

        public DateTime TransactionStart { get; set; }

        public DateTime? TransactionEnd { get; set; }

        [Required]
        [StringLength(50)]
        public string TransactionState { get; set; }

        [StringLength(50)]
        public string Decision { get; set; }

        [StringLength(4)]
        public string ReasonCode { get; set; }

        public string ResponseData { get; set; }

        [StringLength(50)]
        public string ReconciliationID { get; set; }

        [StringLength(1024)]
        public string BillingCompanyName { get; set; }

        [StringLength(1024)]
        public string BillingAddress1 { get; set; }

        [StringLength(1024)]
        public string BillingAddress2 { get; set; }

        [StringLength(1024)]
        public string BillingAddress3 { get; set; }

        [StringLength(1024)]
        public string BillingCity { get; set; }

        [StringLength(1024)]
        public string BillingRegion { get; set; }

        [StringLength(1024)]
        public string BillingCountry { get; set; }

        [StringLength(20)]
        public string BillingPostcode { get; set; }

        public Guid MerchantSourceId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblProductPurchaseSummary> tblProductPurchaseSummary { get; set; }
    }
}
