﻿using CGMAPurchases.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CGMAPurchases.Controllers
{
    public class HomeController : Controller
    {

        private PurchasesContent db = new PurchasesContent();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
 
        [HttpPost] 
        public JsonResult GetPurchasesByEmail(string email, string searchType)
        {
            var query = GetQueryData(email, searchType);
            db.Configuration.ProxyCreationEnabled = false;
            return Json(query);



            //SELECT pps.cimacontactid, pps.firstname, pps.lastname, pps.emailaddress, ppi.productcode, pps.DateCreated, TransactionState
            //  FROM[dbCIMA_Extras_Refresh].[dbo].[tblProductPurchaseSummary]
            //        pps
            //left outer join[dbCIMA_Extras_Refresh].[dbo].[tblProductPurchaseItem]
            //        ppi on pps.Id = ppi.purchaseid
            //left outer join[dbCIMA_Extras_Refresh].[dbo].[tblProductPurchaseSummary_to_tblTransaction]
            //        j on pps.id = j.purchaseid
            //left outer join[dbCIMA_Extras_Refresh].[dbo].[tblTransaction]
            //        t on j.transactionid = t.transactionid

            //WHERE CIMAContactId = '1-9XPEMP'

        }

        public IQueryable GetQueryData(string email, string searchType)
        {
            var query = from purchases in db.tblProductPurchaseSummary.Where(p => (searchType == "cimaid" ? p.CIMAContactId.Contains(email) : searchType == "email" ? p.EmailAddress.Contains(email) : p.EmailAddress.Contains(email)))
                        from transactions in db.tblTransaction.Where(p => p.tblProductPurchaseSummary.Contains(purchases))
                        select new SearchResultViewModel
                        {
                            PurchaseId = purchases.Id,
                            EmailAddress = purchases.EmailAddress,
                            ProductPurchaseId = purchases.ProductPurchaseId,
                            ProductPurchaseName = purchases.ProductPurchaseName,
                            ProductPurchaseType = purchases.ProductPurchaseType,
                            TransactionID = transactions.TransactionID,
                            TransactionState = transactions.TransactionState,
                            TransactionRef = transactions.TransactionRef,
                            DateCreated = purchases.DateCreated,
                            DateUpdated = purchases.DateUpdated,
                            CIMAContactId = purchases.CIMAContactId
                        };


            return query;
        }
    }
}